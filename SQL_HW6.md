// q1
SELECT ROUND(AVG(rental_rate),3) from film;

// q2
SELECT COUNT(*) from film
WHERE title LIKE 'C%';

// q3
SELECT MAX(length) from film
WHERE rental_rate = 0.99;

// q4
SELECT COUNT(DISTINCT replacement_cost) from film
WHERE length > 150;